---
title: MsFPOP
author: Arnaud Liehrmann
date: September 1, 2022
output: html_document
---

> [Introduction](#intro)

> [Installation Notes](#instal)

<a id="intro"></a>

## Introduction

Ms.FPOP is an exact segmentation algorithm that extends functionnal pruning 
ideas from FPOP to a least squares criterion with a multiscale penalty. This
penalty promotes well spread changepoints.

* Details regarding the multiscale penalty can be found [here](https://arxiv.org/abs/2010.11470).
* Details regarding the FPOP algorithm can be found [here](https://doi.org/10.1007/s11222-016-9636-3).

<a id="instal"></a>

## Installation Notes 

### Step 1: Install the devtools package

```
install.packages("devtools")
```

### Step 2: Install the `MsFPOP` package from GitHub

```
library(devtools)
install_github("aLiehrmann/MsFPOP")
```

### Step 3: Load the package

```
library(MsFPOP)
```

<a id="start"></a>

## Quick Start

### Create a new signal

```
set.seed(2024)
bkp0   = c(0, 138, 225, 242, 299, 308, 332, 497)
len    = ceiling(300/min(diff(bkp0)))*diff(bkp0)
mu     = c(-0.18, 0.08, 1.07, -0.53, 0.16, -0.69,-0.16)
sigma  = 0.3 
n      = sum(len)
signal = rep(mu, len) + rnorm(n, sd=sigma)
print(cusum(len))
```

```
# [1]  4692  7650  8228 10166 10472 11288 16898
```
### Estimate the position of changepoints

```
beta  = 2.25 # recommended value
alpha = 9 + beta * log(n) # recommended value
res <- MsFPOP(y=signal, alpha=alpha, beta=beta)
print(res)
```

```
# $changepoints
# [1]  4692  7650  8228 10166 10472 11288 16899
#
# $beta
# [1] 2.25
#
# $alpha
# [1] 30.90364

```




